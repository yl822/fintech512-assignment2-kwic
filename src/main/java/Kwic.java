package assignment2;

import java.util.*;

public class Kwic {
	String[] ignoredWords;
	String[] titles;
	Title[] finalTitles;
	String[] keyTitles;

	public void read(){
		String input = """
                is
                in
                the
                ::
                The Man in The Moon
                The Man and The Mango
                """;
		String[] inputs = input.split("\n");
		String[] ignoredWordsTemp = new String[200];
		String[] titlesTemp = new String[200];
		int switchArray = 0;
		int j = 0;
		for (int i = 0; i < inputs.length; i++) {
			if (inputs[i].equals("::")) {
				switchArray = 1;
				j = i;
			}
			else if (switchArray == 0){
				ignoredWordsTemp[i] = inputs[i].toLowerCase();
			}
			else {
				titlesTemp[i-j-1] = inputs[i].toLowerCase();
			}
		}
		ignoredWords = new String[j];
		titles = new String[inputs.length-j-1];
		for (int i = 0; i < ignoredWords.length; i++){
			ignoredWords[i] = ignoredWordsTemp[i];
		}
		for (int i = 0; i < titles.length; i++){
			titles[i] = titlesTemp[i];
		}
	}

	public void remove() {
		for(int i = 0; i < titles.length; i++){
			for(int j = 0; j < ignoredWords.length; j++) {
				while (titles[i].contains(ignoredWords[j])) {
					titles[i] = titles[i].replaceFirst(ignoredWords[j], "");
				}
			}
			titles[i] = (titles[i]).replaceAll("\s+", " ").trim();
		}
	}

	public void refactor() {
		int sum = 0;
		int[] numbOfWordsInTitle = new int[titles.length];
		for(int i = 0; i < titles.length; i++){
			int temp = titles[i].split(" ").length;
			sum += temp;
			numbOfWordsInTitle[i] = temp;
		}
		finalTitles = new Title[sum];
		int k = 0;
		for(int i = 0; i < titles.length; i++){
			for(int j = 0; j < numbOfWordsInTitle[i]; j++){
				String[] words = titles[i].split(" ");
				words[j] = words[j].toUpperCase();
				String temp = "";
				for(String str : words) {
					temp += str + " ";
				}
				finalTitles[k] = new Title(temp.trim(), words[j]);
				k++;
			}
		}
		keyTitles = new String[sum];
		for(int i = 0; i < sum; i++){
			keyTitles[i] = finalTitles[i].keyTitle;
		}
	}

	public void sortTitles() {
		Arrays.sort(finalTitles, (a, b) -> a.keyword.compareTo(b.keyword));
		for(int i = 0; i < keyTitles.length; i++){
			keyTitles[i] = finalTitles[i].keyTitle;
		}
	}
}

class Title {
	public String keyTitle;
	public String keyword;
	Title(String keyTitle, String keyword) {
		this.keyTitle = keyTitle;
		this.keyword = keyword;
	}
}