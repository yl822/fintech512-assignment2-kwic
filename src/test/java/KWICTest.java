package assignment2;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.DisplayName;

class KWICTest {
    assignment2.Kwic text = new assignment2.Kwic();

    @Test
    @DisplayName("Read")
    void testRead() {
        text.read();
        String[] expectedIgnoredWords = {"is", "in", "the"};
        String[] expectedTitles = {"the man in the moon", "the man and the mango"};
        assertArrayEquals(expectedIgnoredWords, text.ignoredWords);
        assertArrayEquals(expectedTitles, text.titles);
    }

    @Test
    @DisplayName("Remove")
    void testRemove() {
        text.read();
        text.remove();
        String[] expectedTitles = {"man moon", "man and mango"};
        assertArrayEquals(expectedTitles, text.titles);
    }

    @Test
    @DisplayName("Refactor")
    void testRefactor() {
        text.read();
        text.remove();
        text.refactor();
        String[] expectedTitles = {"MAN moon", "man MOON", "MAN and mango", "man AND mango", "man and MANGO"};
        assertArrayEquals(expectedTitles, text.keyTitles);
    }

    @Test
    @DisplayName("SortTitles")
    void testSortTitles() {
        text.read();
        text.remove();
        text.refactor();
        text.sortTitles();
        String[] expectedTitles = {"man AND mango", "MAN moon", "MAN and mango", "man and MANGO", "man MOON"};
        assertArrayEquals(expectedTitles, text.keyTitles);
    }

}
